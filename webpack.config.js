__webpack_base_uri__ = "http://localhost:8080";
const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: path.resolve(__dirname, "./src/index.js"),
  output: {
    path: path.resolve(__dirname, "public"),
    publicPath: "/js/",
    filename: "main.js",
    sourceMapFilename: "main.js.map",
  },

  module: {
    rules: [
      {
        test: /\.jsx?$|\.es6$|\.js$/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/react"],
          },
        },
        exclude: /(node_modules|bower_components)/,
      },
      {
        test: /\.(scss|css)$/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "dist/assets/images/",
            },
          },
        ],
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
          },
        ],
      },
      {
        test: /\.(mp3|wav|wma|ogg)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "[name].[contenthash].[ext]",
            outputPath: "assets/audio/",
            publicPath: "assets/audio/",
          },
        },
      },
    ],
  },

  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "bundle.js",
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      title: "Happy Holidays",
      template: "./public/index.html",
    }),
  ],
  devServer: {
    static: path.resolve(__dirname, "./build"),
    hot: true,
  },
  externals: {
    transition: "Transition",
  },
};
