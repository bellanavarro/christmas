import React, { useState, useEffect, useRef } from "react";
import "./assets/styles/App.scss";
import {
  Col,
  Container,
  Row,
  Modal,
  Button,
  Form,
  Card,
  Carousel,
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import santa from "./assets/images/santa.png";
import cover from "./assets/images/cover.jpg";
import cardcover from "./assets/images/card-cover.jpg";
import modalImg from "./assets/images/christmas-loading.jpg";
import quotes from "./assets/content/quotes.json";

// Songs
import whitechristmas from "./assets/audio/whitechristmas.mp3";
import homeforchristmas from "./assets/audio/homeforchristmas.mp3";
import littlechristmas from "./assets/audio/littlechristmas.mp3";
import chrissong from "./assets/audio/chrissong.mp3";
import letitsnow from "./assets/audio/letitsnow.mp3";
import bluechris from "./assets/audio/bluechris.mp3";
import jingle from "./assets/audio/jingle.mp3";
import timeoftheyear from "./assets/audio/timeoftheyear.mp3";
import feliznavidad from "./assets/audio/feliznavidad.mp3";

// import { Transition } from "react-transition-group";

import {
  ThreeDots,
  ChevronLeft,
  PauseFill,
  PlayFill,
  SkipBackwardFill,
  SkipForwardFill,
  MusicNote,
  X,
} from "react-bootstrap-icons";

// const Transition = require("https://cdn.skypack.dev/react-transition-group@4.4.1");

function App() {
  // Form
  const [validated, setValidated] = useState(false);
  const [name, switchName] = useState("");

  // Modal Functions
  const [show, setShow] = useState(true);
  const [showRecipe, setShowRecipe] = useState(false);
  const [showRecipeCavallucci, setShowRecipeCavallucci] = useState(false);

  // Toggle Music Container
  const [showMusic, setShowMusic] = useState(true);

  // Modal
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const tracks = [
    {
      name: "White Christmas",
      artist: "Bing Crosby",
      cover: "https://m.media-amazon.com/images/I/61PB9M1B3TL.jpg",
      source: whitechristmas,
    },
    {
      name: "I'll Be Home For Christmas",
      artist: "Bing Crosby",
      cover: "https://m.media-amazon.com/images/I/91wIWrOLvUL._SX355_.jpg",
      source: homeforchristmas,
    },
    {
      name: "Have Yourself a Merry Little Christmas",
      artist: "Judy Garland",
      cover: "https://angartwork.akamaized.net/webp/?id=123088949&size=296",
      source: littlechristmas,
    },
    {
      name: "The Christmas Song",
      artist: "Nat King Cole",
      cover: "https://m.media-amazon.com/images/I/61qxdGsqYIL._SL1115_.jpg",
      source: chrissong,
    },
    {
      name: "Let It Snow! Let It Snow! Let It Snow!",
      artist: "Frank Sinatra",
      cover: "https://m.media-amazon.com/images/I/51P9UtVVdAL._SS500_.jpg",
      source: letitsnow,
    },
    {
      name: "Blue Christmas",
      artist: "Elvis Presley's",
      cover: "https://m.media-amazon.com/images/I/61-XBiXkFwL._SX355_.jpg",
      source: bluechris,
    },
    {
      name: "Jingle Bells",
      artist: "",
      cover: "https://images-na.ssl-images-amazon.com/images/I/A1JJEFW4QvL.jpg",
      source: jingle,
    },
    {
      name: "It's the Most Wonderful Time of the Year",
      artist: "Andy Williams",
      cover: "https://m.media-amazon.com/images/I/71VPBspvekL._SS500_.jpg",
      source: timeoftheyear,
    },
    {
      name: "Feliz Navidad",
      artist: "José Feliciano",
      cover: "https://m.media-amazon.com/images/I/71a2ElIQDsL._SS500_.jpg",
      source: feliznavidad,
    },
  ];

  const AudioControls = ({
    isPlaying,
    onPlayPauseClick,
    onPrevClick,
    onNextClick,
  }) => (
    <div className="controls">
      <button className="controlButton" onClick={onPrevClick}>
        <SkipBackwardFill />
      </button>
      {isPlaying ? (
        <button
          className="centerButton"
          onClick={() => onPlayPauseClick(false)}
        >
          <PauseFill />
        </button>
      ) : (
        <button className="centerButton" onClick={() => onPlayPauseClick(true)}>
          <PlayFill />
        </button>
      )}
      <button className="controlButton" onClick={onNextClick}>
        <SkipForwardFill />
      </button>
    </div>
  );

  // Music
  const AudioPlayer = (track) => {
    // State
    const [isPlaying, setIsPlaying] = useState(false);
    const [trackIndex, setTrackIndex] = useState(0);
    const [trackProgress, setTrackProgress] = useState(0);
    const { name, artist, cover, source } = tracks[trackIndex];

    // Refs
    const audioRef = useRef(new Audio(source));
    const intervalRef = useRef();
    const isReady = useRef(false);

    // Destructure for conciseness
    const { duration } = audioRef.current;

    const toPrevTrack = () => {
      if (trackIndex - 1 < 0) {
        setTrackIndex(tracks.length - 1);
      } else {
        setTrackIndex(trackIndex - 1);
      }
    };

    const toNextTrack = () => {
      if (trackIndex < tracks.length - 1) {
        setTrackIndex(trackIndex + 1);
      } else {
        setTrackIndex(0);
      }
    };
    useEffect(() => {
      if (isPlaying) {
        audioRef.current.play();
        startTimer();
      } else {
        audioRef.current.pause();
      }
    }, [isPlaying]);

    useEffect(() => {
      // Pause and clean up on unmount
      return () => {
        audioRef.current.pause();
        clearInterval(intervalRef.current);
      };
    }, []);

    // Handle setup when changing tracks
    useEffect(() => {
      audioRef.current.pause();

      audioRef.current = new Audio(source);
      setTrackProgress(audioRef.current.currentTime);

      if (isReady.current) {
        audioRef.current.play();
        setIsPlaying(true);
        startTimer();
      } else {
        // Set the isReady ref as true for the next pass
        isReady.current = true;
      }
    }, [trackIndex]);

    const startTimer = () => {
      //   // Clear any timers already running
      clearInterval(intervalRef.current);

      intervalRef.current = setInterval(() => {
        if (audioRef.current.ended) {
          toNextTrack();
        } else {
          setTrackProgress(audioRef.current.currentTime);
        }
      }, [1000]);
    };
    const onScrub = (value) => {
      // Clear any timers already running
      clearInterval(intervalRef.current);
      audioRef.current.currentTime = value;
      setTrackProgress(audioRef.current.currentTime);
    };

    const onScrubEnd = () => {
      // If not already playing, start
      if (!isPlaying) {
        setIsPlaying(true);
      }
      startTimer();
    };
    const currentPercentage = duration
      ? `${(trackProgress / duration) * 100}%`
      : "0%";
    const trackStyling = `
  -webkit-gradient(linear, 0% 0%, 100% 0%, color-stop(${currentPercentage}, #fff), color-stop(${currentPercentage}, #777))
`;

    return (
      <div className="playerContaier">
        <img src={cover} className="avatar1" />
        <img src={cover} className="avatar" />
        <h4 className="name">{artist}</h4>
        <h1 className="title">{name}</h1>
        <input
          type="range"
          value={trackProgress}
          step="1"
          min="0"
          max={duration ? duration : `${duration}`}
          className="progress"
          onChange={(e) => onScrub(e.target.value)}
          onMouseUp={onScrubEnd}
          onKeyUp={onScrubEnd}
          style={{ background: trackStyling }}
        />
        <AudioControls
          isPlaying={isPlaying}
          onPrevClick={toPrevTrack}
          onNextClick={toNextTrack}
          onPlayPauseClick={setIsPlaying}
        />
      </div>
    );
  };

  function Header() {
    return (
      <div className="header">
        <button className="icon" onClick={(x) => setShowMusic(false)}>
          <ChevronLeft />
        </button>
        <h1 className="headerText">Song</h1>
        <button
          className="icon"
          style={{ color: "transparent", cursor: "default" }}
        >
          <ThreeDots />
        </button>
      </div>
    );
  }
  // Submit Modal function
  const handleSubmit = (event) => {
    const form = event.currentTarget;

    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      handleClose();

      event.stopPropagation();
      event.preventDefault();
    }
    setValidated(true);

    event.stopPropagation();
    event.preventDefault();
  };

  // Snowflakes
  function createDots() {
    let components = [];
    for (let i = 1; i < 50; i++) {
      components.push(<div className="snowflake" key={i}></div>);
    }
    return components;
  }

  // Card
  function openCard() {
    var button = document.getElementById("button-christmas");

    if (button.innerHTML === "Open Card") {
      button.innerHTML = "Close Card";
    } else {
      button.innerHTML = "Open Card";
    }

    var cards = document.querySelectorAll(".card");
    for (var i = 0; i < cards.length; i++) {
      cards[i].classList.toggle("open");
    }
  }
  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  // Calendar
  function Calendar() {
    let calendar = [];
    for (let i = 1; i <= 31; i++) {
      calendar.push(
        <div key={i.toString()}>
          <div className="circle">
            <div className="circle-inner">
              <div className={`circle-front circle_${i.toString()}`}>
                <h1>{i}</h1>
              </div>
              <div className={`circle-back circle_${i.toString()}`}>
                <p>
                  {quotes[i].quote} - {quotes[i].author}
                </p>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return calendar;
  }

  return (
    <div className="App">
      {createDots()}
      {/* Modal Input Name */}
      <Modal show={show} centered>
        <Modal.Header>
          <img src={modalImg} className="w-100" />
          <Modal.Title>How would you like to be called?</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Form noValidate validated={validated} onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="name">
              <Form.Control
                required
                type="text"
                size="md"
                placeholder="Name or Nickname"
                value={name}
                onChange={(e) => switchName(e.target.value)}
                minLength={3}
              />
              <Form.Control.Feedback type="invalid" style={{ color: "white" }}>
                Come'on! You do have a name, don't you?
              </Form.Control.Feedback>
            </Form.Group>
            <div
              className="button-modal"
              style={{ display: "flex", justifyContent: "center" }}
            >
              <Button variant="primary" type="submit">
                Go!
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
      {/* Modal Recipe */}
      <Modal
        show={showRecipe}
        centered
        size="lg"
        aria-labelledb
        className="recipe-modal"
      >
        <Modal.Header>
          <img
            src="https://celebratingsweets.com/wp-content/uploads/2018/12/Homemade-Hot-Chocolate-3.jpg"
            className="w-100"
          />
          <button
            className="open-music icon"
            onClick={(x) => setShowRecipe(false)}
          >
            <X size={50} />
          </button>
          <Modal.Title>Hot Chocolate</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Row className="recipe-wrap">
            <Col>
              <h1>Recipe</h1>
              <p>
                Microwave the chopped chocolate in 30 second intervals, stirring
                after each time, until melted. You can also melt the chocolate
                in bain marie.
              </p>
              <p>
                Heat the milk up to 180ºC and don't let it boil. Some small
                bubbles will start to form on the side of the pot. Add a
                sprinkle of salt. Tip: a sprinkle of sugar will make it
                fluffier.
              </p>
              <p>
                When the milk is hot, turn off the heat and add the melted
                chocolate into the milk.
              </p>
              <p>
                Bella's touch: I use unsweetened almond milk and I like using
                vanilla sugar in the milk. I also add a pinch of nutmeg.
              </p>
              <p>
                Add your toppings! Marshmallows, whipped cream, toffee bits,
                coarse sea salt, crushed spekulatios. Anything you fancy!
              </p>
            </Col>
            <Col>
              <h1>Ingredients</h1>
              <div>
                <ul>
                  <li>Chopped chocolate or chocolate drops</li>
                  <li>Milk or Alternative</li>
                  <li>Pinch of Salt</li>
                  <li>Vanilla Sugar or Common Sugar (optional)</li>
                  <li>Nutmeg (optional)</li>
                  <li>Toppings of choice</li>
                </ul>
              </div>
            </Col>
          </Row>
        </Modal.Body>
      </Modal>
      {/* Modal Recipe */}
      <Modal
        show={showRecipeCavallucci}
        centered
        size="lg"
        aria-labelledb
        className="recipe-modal"
      >
        <Modal.Header>
          <img
            src="https://images.wsj.net/im-443298?width=700&size=1.5&pixel_ratio=1.1"
            className="w-100"
          />
          <button
            className="open-music icon"
            onClick={(x) => setShowRecipeCavallucci(false)}
          >
            <X size={50} />
          </button>
          <Modal.Title>Cavallucci</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Row className="recipe-wrap">
            <Col>
              <h1>Recipe</h1>
              <ul>
                <li>Heat oven to 340 degrees.</li>
                <li>
                  In a small saucepan over low heat, cook sugar in ⅓ cup plus 1
                  tablespoon water. Once sugar dissolves, bring liquid to a
                  simmer and cook, stirring regularly with rubber spatula to
                  prevent sticking, until it reduces very slightly but does not
                  color, about 5 minutes. Remove from heat and stir in walnuts,
                  candied orange and spices.
                </li>
                <li>
                  Sift flour into a separate bowl. Add hot syrup and mix
                  together to form a dough. On a work surface well-dusted with
                  flour, roll apricot-size dough balls. Place balls on a baking
                  sheet lined with parchment paper and pat to flatten slightly.
                  Bake until cavallucci are hard to the touch but still
                  flour-white, 15 minutes. They’re meant to have a hard shell
                  but a soft interior, so do not bake them too long. You can
                  bake a little longer for a crunchier cookie, but be careful
                  not to let them color too much.
                </li>
                <li>
                  Let cookies cool on a rack before serving. Serve with wine,
                  preferably vin santo, for dunking. Stored in an airtight
                  container at room temperature, they should last 3 weeks.
                </li>
              </ul>
            </Col>
            <Col>
              <h1>Ingredients</h1>
              <div>
                <ul>
                  <li>1½ cups sugar</li>
                  <li>1 tablespoon honey</li>
                  <li>1 cup freshly shelled walnuts, finely chopped</li>
                  <li>1¾ ounces candied orange rind, finely chopped</li>
                  <li>2 tablespoons anise seeds</li>
                  <li>2 teaspoons ground cinnamon</li>
                  <li>2 cups flour, plus more for dusting</li>
                </ul>
              </div>
            </Col>
          </Row>
        </Modal.Body>
      </Modal>

      <Container fluid style={{ padding: 0 }}>
        {/* Music Container */}
        {show ? (
          ""
        ) : (
          <div>
            <div className={`music-container ${showMusic ? "showing" : ""}`}>
              <Header />
              <AudioPlayer />
            </div>

            <div className={`toggle-music ${showMusic ? "" : "showing"}`}>
              <button
                className="open-music icon"
                onClick={(x) => setShowMusic(true)}
              >
                <MusicNote size={30} />
              </button>
            </div>
          </div>
        )}

        {/* Header */}
        <div className="cover-wrapper">
          <img src={cover} className="w-100 cover-img" />
          <div className="text-wrapper">
            <h1 className="cover-text-one">
              It's the most beautiful time of the year...
            </h1>
            <div className="scroll-downs">
              <div className="mousey">
                <div className="scroller"></div>
              </div>
            </div>
          </div>
        </div>
        {/* <Row className="App-header">
          <Col xs={12} md={6} className="santa-col">
            <img src={santa} width="550" fluid />
          </Col>
          <Col xs={12} md={6}>
            <h1>Ho Ho Ho!</h1>{" "}
            <h1>
              Merry Christmas <span className="name">{name}</span>!
            </h1>
          </Col>
        </Row> */}
        {/* Card-Desktop */}
        <div className="card-section">
          {!show ? <h1>{capitalizeFirstLetter(name)}'s Special Card</h1> : ""}

          <div className="card-wrap">
            <div className="book">
              <div className="page">
                <div
                  className="page__1"
                  style={{ backgroundImage: `url(${cardcover})` }}
                >
                  <div className="card-text-cover">
                    <h1>Merry Christmas!</h1>
                  </div>
                </div>
                <div className="page__2">
                  <img src={santa} width="350" fluid />
                  <div className="card-text-wrap">
                    <p>Dear {name}, </p>
                    <p>
                      Wishing you and your loved ones, happiness, peace and
                      prosperity this Christmas and in the coming New Year.
                    </p>
                    <h1>Bella</h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Second Content */}
        <div>
          <div className="first-content">
            <h1 style={{ marginBottom: "15px" }}>Your Christmas Space</h1>
            <h1>Recipes</h1>
            <h2>
              During these difficult times is more important than ever to put on
              work to make it feel like Christmas at home!
            </h2>
            <h2>
              Bring a new touch to your Christmas this year with these
              international recipes I selected specially for you!
            </h2>
            <Row className="card-rows g-4" xs={1} sm={2} md={3}>
              <Col>
                <Card>
                  <Card.Img
                    variant="top"
                    src="https://images.lecker.de/platzchenteig-rezept,id=944d330e,b=lecker,w=610,cg=c.jpg"
                  />
                  <Card.Body>
                    <Card.Title>
                      Klassischer Plätzchenteig - Sugar Cookies (in German)
                    </Card.Title>
                    <Card.Text>
                      The classic cookies. With just flour, eggs, butter and
                      sugar you can start making these delicious treats!
                    </Card.Text>
                    <div className="button-wrapper">
                      <a
                        href="https://www.lecker.de/klassischer-plaetzchenteig-80449.html"
                        target="_blank"
                      >
                        <Button variant="primary">Gimme that sugar!</Button>
                      </a>
                    </div>
                  </Card.Body>
                </Card>
              </Col>
              <Col>
                <Card>
                  <Card.Img
                    variant="top"
                    src="https://d12xickik43a9a.cloudfront.net/images/magazine/de/M75755-Rumkugeln_74304-Q75-750.jpg"
                  />
                  <Card.Body>
                    <Card.Title>Rum Balls (in German)</Card.Title>
                    <Card.Text>
                      Little balls of rum pleasure. Easy to make, hard to stop
                      eating.
                    </Card.Text>
                    <div className="button-wrapper">
                      <a
                        href="https://www.chefkoch.de/rezepte/315151113038089/Claudias-Rumkugeln.html"
                        target="_blank"
                      >
                        <Button variant="primary">Rum? Count me in!</Button>
                      </a>
                    </div>
                  </Card.Body>
                </Card>
              </Col>
              <Col>
                <Card>
                  <Card.Img
                    variant="top"
                    src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/delish-200813-chocolate-kisses-snowball-cookies-3033-portrat-yaedit-jpg-1633971438.jpg"
                  />
                  <Card.Body>
                    <Card.Title>
                      Chocolate Kisses Snowball Cookies (in English)
                    </Card.Title>
                    <Card.Text>
                      Hidden inside these snowball cookies are chocolate kisses
                      for the sweetest surprise. Little drops of heaven.
                    </Card.Text>
                    <div className="button-wrapper">
                      <a
                        href="https://www.delish.com/cooking/recipe-ideas/a30149192/chocolate-kisses-snowball-cookies-recipe/"
                        target="_blank"
                      >
                        <Button variant="primary">Give me kisses!</Button>
                      </a>
                    </div>
                  </Card.Body>
                </Card>
              </Col>
              <Col>
                <Card>
                  <Card.Img
                    variant="top"
                    src="https://images.wsj.net/im-443298?width=700&size=1.5&pixel_ratio=1.1"
                  />
                  <Card.Body>
                    <Card.Title>Italian Cavallucci (in English)</Card.Title>
                    <Card.Text>
                      Bring the taste of the Italian Christmas to your home with
                      these little balls made with walnuts, candied orange,
                      anise and cinnamon. They are delicious dipped in Vin
                      Santo!
                    </Card.Text>
                    <div className="button-wrapper">
                      <Button
                        variant="primary"
                        onClick={(x) => setShowRecipeCavallucci(true)}
                      >
                        Mamma Mia!
                      </Button>
                    </div>
                  </Card.Body>
                </Card>
              </Col>
              <Col>
                <Card>
                  <Card.Img
                    variant="top"
                    src="https://img.elo7.com.br/product/zoom/2C518F1/camafeu-camafeu-de-nozes.jpg"
                  />
                  <Card.Body>
                    <Card.Title>
                      Bella's Favorite - Camafeu / Cameo Bombons (in German)
                    </Card.Title>
                    <Card.Text>
                      This is my favourite sweet from Brazil! I found a recipe
                      in English with a more traditional touch and an adapted
                      german version that can be made in 15 minutes.
                    </Card.Text>
                    <Row>
                      <Col className="button-wrapper">
                        <a
                          href="https://www.mytasteat.com/r/ein-brasilianischer-klassiker-weihnachts-cameo-bonbons-11964313.html"
                          target="_blank"
                        >
                          <Button variant="primary">
                            Adapted German Version
                          </Button>
                        </a>
                      </Col>
                      <Col className="button-wrapper">
                        <a
                          href="https://www.oliviascuisine.com/brazilian-walnut-candy/"
                          target="_blank"
                        >
                          <Button variant="primary">Traditional</Button>
                        </a>
                      </Col>
                    </Row>
                  </Card.Body>
                </Card>
              </Col>
              <Col>
                <Card>
                  <Card.Img
                    variant="top"
                    src="https://www.fifteenspatulas.com/wp-content/uploads/2015/01/Hot-Chocolate-Recipe-Fifteen-Spatulas-2-640x427.jpg"
                  />
                  <Card.Body>
                    <Card.Title>Hot Chocolate</Card.Title>
                    <Card.Text>
                      What is a winter without a warm cup of hot chocolate? It's
                      so easy to do and it puts a smile in the face of anyone! I
                      put down a traditional recipe for you with a bit of my
                      personal touch. :)
                    </Card.Text>
                    <div className="button-wrapper">
                      <Button
                        variant="primary"
                        onClick={(x) => setShowRecipe(true)}
                      >
                        Warm me up!
                      </Button>
                    </div>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </div>
        </div>
        {/* Second Content - DIY*/}
        <div>
          <div className="second-content">
            <h1>Inspiration</h1>
            <h2>
              I found some beautiful christmas decorations to inspire you to get
              your hands busy!
            </h2>
            <Carousel fade>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src="https://hips.hearstapps.com/hbu.h-cdn.co/assets/16/46/burlap-christmas-wreath-loveoffamilyandhome.jpg"
                  alt="First slide"
                />
                <Carousel.Caption>
                  <h3>Neutral Burlap Wreath</h3>
                  <p>
                    A burst of pinecones, pine, and a sparkly snowflake make the
                    rustic holiday wreath of our dreams.
                  </p>
                </Carousel.Caption>
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src="https://thelovelydrawer.com/wp-content/uploads/2015/12/December-styling-the-seasons3.jpg"
                  alt="Second slide"
                />

                <Carousel.Caption>
                  <h3>Spare Little Tree</h3>
                  <p>
                    This adorable "tree" will look chic on any surface around
                    your home, and it's easy to make. Just find a sturdy tree
                    branch, apply spray paint, set it in a small glass bottle or
                    vase, and decorate!
                  </p>
                </Carousel.Caption>
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/christmas-decoration-ideas-pillow-1598219404.jpg?crop=1xw:1xh;center,top&resize=980:*"
                  alt="Third slide"
                />

                <Carousel.Caption>
                  <h3>Add a Festive Pillow</h3>
                  <p>
                    Watching your favorite holiday movies will be way more
                    enjoyable with a cozy throw pillow in the mix. Top a red
                    pillow with white ribbon and a bit of greenery to enliven
                    any basic chair or sofa.
                  </p>
                </Carousel.Caption>
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/itallstartedwithpaint-christmas-decoration-ideas-1535636690.jpg?crop=0.869xw:0.869xh;0.0207xw,0.112xh&resize=768:*&keepGifs=1"
                  alt="Third slide"
                />

                <Carousel.Caption>
                  <h3>Hang Mini Wreaths</h3>
                  <p>
                    Simple and charming. The more, the merrier, especially when
                    it comes to mini wreaths.
                  </p>
                </Carousel.Caption>
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src="https://hips.hearstapps.com/ghk.h-cdn.co/assets/17/45/wine-bottle-candle-holders.jpg?crop=0.8915343915343915xw:1xh;center,top&resize=980:*"
                  alt="Third slide"
                />

                <Carousel.Caption>
                  <h3>Repurpose Wine Bottles</h3>
                  <p>
                    Raid the recycling bin for a new set of festive
                    candlesticks. Just replace the labels with silvery wrapping
                    paper and luxe ribbon before adding tapers.
                  </p>
                </Carousel.Caption>
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/christmas-decoration-ideas-pillow-1598219404.jpg?crop=1xw:1xh;center,top&resize=980:*"
                  alt="Third slide"
                />

                <Carousel.Caption>
                  <h3>Add a Festive Pillow</h3>
                  <p>
                    Watching your favorite holiday movies will be way more
                    enjoyable with a cozy throw pillow in the mix. Top a red
                    pillow with white ribbon and a bit of greenery to enliven
                    any basic chair or sofa.
                  </p>
                </Carousel.Caption>
              </Carousel.Item>
            </Carousel>
          </div>
        </div>
        {/* Calendar */}
        <div className="cal-section">
          <h1>Your Special Calendar</h1>
          <h2>
            Hover your mouse on today's date to get your special festive
            message! On mobile, you can click on it!
          </h2>
          <Row className="cal-wrapper">
            <Calendar />
          </Row>
        </div>
        {/* Footer */}
        <div className="footer">
          <p>
            Developed by{" "}
            <a href="https://bella-capilla.com/" target="_blank">
              Isabella Capilla
            </a>
          </p>
          <p>2021</p>
        </div>
      </Container>
    </div>
  );
}

export default App;
